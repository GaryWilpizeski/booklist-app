# booklist-app

A JavaScript book list app without any library or JS framework, using classes, local storage and more.

Also testing CI/CD with GitLab CI and making sure everything with git is all set.

[View the Booklist App here](https://garywilpizeski.gitlab.io/booklist-app/ "Booklist App")
